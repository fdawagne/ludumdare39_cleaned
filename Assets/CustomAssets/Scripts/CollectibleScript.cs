﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof (Collider))]
public class CollectibleScript : MonoBehaviour
{
    public int ObjID;
    public Sprite ObjSprite;
    bool _glowing = false;
    private Behaviour halo;

    public bool glowing
    {
        get
        {
            return _glowing;
        }
        set
        {
            _glowing = value;
        }
    }

    private void Awake()
    {
        gameObject.tag = "Collectible";
        GetComponent<Collider>().isTrigger = true;
    }

    // Use this for initialization
    void Start()
    {
        halo = (Behaviour)gameObject.GetComponent("Halo");
    }

    // Update is called once per frame
    void Update()
    {
        halo.enabled = glowing;
    }

    public void PickUp()
    {
        SystemValues sysVal = GameObject.FindGameObjectWithTag("GameController").GetComponent<SystemValues>();
        sysVal.UI_elem_inventory.GetComponent<UI_inventory>().activateImage(ObjID, ObjSprite);
        sysVal.inventory[ObjID] = true;
        Destroy(gameObject);
    }
}

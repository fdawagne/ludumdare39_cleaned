﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof (Collider))]
public class InteractableScript : MonoBehaviour {

    public bool isActive = false;

	// Use this for initialization
	void Awake () {
        gameObject.tag = "Interactable";
        GetComponent<Collider>().isTrigger = true;
    }
	
	// Update is called once per frame
	public void activateObject() {
        GetComponent<Animator>().SetTrigger("activateObject");
    }

    public void playSoundAndBool()
    {
        isActive = !isActive;
        GetComponent<AudioSource>().Play();
    }
}

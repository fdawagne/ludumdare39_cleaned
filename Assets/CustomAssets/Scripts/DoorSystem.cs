﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorSystem : MonoBehaviour
{

    public int doorID;
    public GameObject key;
    public string typeOfDoor;
    private bool open = false;

    public GameObject roomRoof;

    private GameObject txtZone;

    private void Awake()
    {
        gameObject.tag = "Door";
    }

    // Use this for initialization
    void Start()
    {
        if (key != null && key.gameObject.tag == "Collectible")
        {
            typeOfDoor = "keyDoor";
        }
        else if (key != null && key.gameObject.tag == "Interactable")
        {
            typeOfDoor = "actionDoor";
        }
        else
        {
            typeOfDoor = "unlockedDoor";
        }
        txtZone = GameObject.FindGameObjectWithTag("SubText");
    }

    public void SetDoorID()
    {
        if (key != null && key.gameObject.tag == "Collectible")
        {
            doorID = key.GetComponent<CollectibleScript>().ObjID;
        }
    }

    public void tryToOpen()
    {
        if (!open)
        {
            switch (typeOfDoor)
            {
                case "unlockedDoor":
                    Open();
                    break;
                case "actionDoor":
                    if (key.GetComponent<InteractableScript>().isActive)
                    {
                        this.Open();
                    }
                    else
                    {
                        txtZone.GetComponent<SubtitlesBehavior>().setAs("I need to activate something to pass this door.", Color.white);
                    }
                    break;
                case "keyDoor":
                    if (GameObject.FindGameObjectWithTag("GameController").GetComponent<SystemValues>().inventory[doorID])
                    {
                        this.Open();
                    }
                    else
                    {
                        txtZone.GetComponent<SubtitlesBehavior>().setAs("I need a badge to open this door.", Color.white);
                    }
                    break;
                default:
                    print("type of door unknown, debug in DoorSystem.cs");
                    break;
            }
        }
    }


    public void Open()
    {
        //PLAY ANIMATION HERE
        GetComponent<BoxCollider>().enabled = false;
        GetComponent<Animator>().SetTrigger("OpenDoor");

        if (roomRoof != null)
        {
            roomRoof.GetComponent<RoomCover>().fadeOut();
        }
    }
}

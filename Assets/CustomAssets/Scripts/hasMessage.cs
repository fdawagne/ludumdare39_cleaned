﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class hasMessage : MonoBehaviour
{

    public string message = "PutMessageHere";
    private GameObject txtzone;
    public float timeOnScreen = 5;

    private void Awake()
    {
        gameObject.tag = "hasMessage";
    }

    private void Start()
    {
        txtzone = GameObject.FindGameObjectWithTag("SubText");
    }

    public void read()
    {
        txtzone.GetComponent<SubtitlesBehavior>().setAs(message, Color.white ,timeOnScreen);
    }


}

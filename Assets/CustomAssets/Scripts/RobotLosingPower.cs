﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RobotLosingPower : MonoBehaviour
{

    public float batteryMax = 100;
    public float powerLosingSpeed = 0.5f;
    public float actionModifier;
    public float battery;
    public bool alive = true;
    public float suddenActionCost = 5;
    public GameObject deadRobot;
    public float currentRepairCost;

    private RobotMovement rMove;

    SystemValues sv;

    // Use this for initialization
    void Start()
    {
        alive = true;
        rMove = GetComponent<RobotMovement>();
        sv = GameObject.FindGameObjectWithTag("GameController").GetComponent<SystemValues>();
        battery = batteryMax;
    }

    private void Update()
    {
        if (alive)
        {
            if (sv.repairing != null)
            {
                actionModifier = currentRepairCost;
            }
            else if (rMove.moving && rMove.turning)
            {
                actionModifier = 2f;
            }
            else if (rMove.moving || rMove.turning)
            {
                actionModifier = 1f;
            }
            else
            {
                actionModifier = 0.5f;
            }

            battery -= powerLosingSpeed * Time.deltaTime * actionModifier;

            if (battery <= 0)
            {
                alive = false;
                myDeath();
            }
        }
    }

    public void losePower(float factor = 1f)
    {
        battery -= suddenActionCost * factor;
    }

    private void myDeath()
    {
        GetComponent<RobotInterraction>().enabled = false;
        GetComponent<RobotMovement>().enabled = false;
        StartCoroutine(death());   
    }

    IEnumerator death()
    {
        // UI fade to black
        Color lerpedColor = Color.black;
        lerpedColor.a = 0;
        GameObject myBlackFade = GameObject.FindGameObjectWithTag("MyBlackImage");

        while (lerpedColor.a <= 1)
        {
            lerpedColor.a += 0.01f;
            yield return new WaitForSeconds(0.01f);
            myBlackFade.GetComponent<Image>().color = lerpedColor;
        }

        //Spawn dead body
        Vector3 oldPosition = transform.position;
        Quaternion oldRotation = transform.rotation;

        gameObject.transform.position = GameObject.FindGameObjectWithTag("Respawn").transform.position;
        gameObject.transform.rotation = GameObject.FindGameObjectWithTag("Respawn").transform.rotation;

        Instantiate(deadRobot, oldPosition, oldRotation);

        // UI fade out
        while (lerpedColor.a >= 0)
        {
            lerpedColor.a -= 0.01f;
            yield return new WaitForSeconds(0.01f);
            myBlackFade.GetComponent<Image>().color = lerpedColor;
        }

        sv.robotDeath(gameObject);

        battery = batteryMax;

        GetComponent<RobotInterraction>().enabled = true;
        GetComponent<RobotMovement>().enabled = true;

        alive = true;
    }


}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RobotInterraction : MonoBehaviour
{
    private RobotLosingPower rPower;

    private void Start()
    {
        rPower = GetComponent<RobotLosingPower>();
    }

    void OnTriggerStay(Collider other)
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            switch (other.gameObject.tag)
            {
                case "Collectible":
                    other.GetComponent<CollectibleScript>().PickUp();
                    rPower.losePower();
                    break;
                case "Door":
                    other.GetComponent<DoorSystem>().tryToOpen();
                    rPower.losePower();
                    break;
                case "Interactable":
                    other.GetComponent<InteractableScript>().activateObject();
                    rPower.losePower();
                    break;
                case "Consumable":
                    other.gameObject.GetComponent<Consumable>().ConsumeBattery(gameObject);
                    break;
                case "Repairable":
                    other.GetComponent<Repairable>().startRepairs();
                    break;
                case "hasMessage":
                    other.GetComponent<hasMessage>().read();
                    break;
                case "TPpoint":
                    if (other.isTrigger)
                    {
                        other.GetComponent<teleportPoint>().teleport(gameObject);
                    }
                    break;
                default:
                    break;
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        switch (other.gameObject.tag)
        {
            case "Collectible":
                other.gameObject.GetComponent<CollectibleScript>().glowing = true;
                break;
            case "Consumable":
                other.gameObject.GetComponent<Consumable>().glowing = true;
                break;
            case "Repairable":
                other.GetComponent<Repairable>().closeToBot = true;
                break;
            case "Finish":
                GetComponent<RobotLosingPower>().battery = 0;
                break;
            default:
                break;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        switch (other.gameObject.tag)
        {
            case "Collectible":
                other.gameObject.GetComponent<CollectibleScript>().glowing = false;
                break;
            case "Consumable":
                other.gameObject.GetComponent<Consumable>().glowing = false;
                break;
            case "Repairable":
                other.GetComponent<Repairable>().closeToBot = false;
                break;
            default:
                break;
        }
    }

}

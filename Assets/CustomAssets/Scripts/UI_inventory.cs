﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_inventory : MonoBehaviour
{
    //private GameObject gm;
    public Image defaultImage;
    private Image[] imgTab;
    float initX = -30;
    float initY = -30;
    float offsetY = -50;
    public Sprite testSprite;

    void Awake()
    {
        //gm = GameObject.FindGameObjectWithTag("GameController");
        GameObject[] gm = GameObject.FindGameObjectsWithTag("Collectible");
        InitializeUI(gm.Length);
    }

    public void InitializeUI(int nbr)
    {
        imgTab = new Image[nbr];
        for (int i = 0; i < nbr; i++)
        {
            imgTab[i] = Instantiate(defaultImage);
            RectTransform recttrans = imgTab[i].GetComponent<RectTransform>();
            recttrans.parent = transform.parent;
            recttrans.anchoredPosition= new Vector3 (initX, initY + offsetY*i,0);
            imgTab[i].enabled= false;
        }

        
    }

    public void activateImage(int i, Sprite spr)
    {
            imgTab[i].enabled = true;
            imgTab[i].GetComponent<Image>().sprite = spr;
    }

    // Update is called once per frame
    void Update()
    {
        //test/////////////////////////////////////
        if (Input.GetKeyDown(KeyCode.L))
        {
            activateImage(1, testSprite);
        }

        /////////////////////////////////////////test//
    }
}

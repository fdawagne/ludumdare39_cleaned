﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SystemValues : MonoBehaviour
{
    public bool[] inventory;
    public int NbrOfCollectibles;
    public GameObject UI_elem_inventory;
    public Sprite[] repairSprites;
    public Image repairImg;
    public GameObject repairing;

    private Color colorAI;

    private int nbrOfDeath = 0;

    private SubtitlesBehavior textZone;

    // Use this for initialization
    void Start()
    {
        // Subtitle Zone
        textZone = GameObject.FindGameObjectWithTag("SubText").GetComponent<SubtitlesBehavior>();

        colorAI = new Color(0f, 1f, 0f, 1f);

        // List of Collectibles
        NbrOfCollectibles = GameObject.FindGameObjectsWithTag("Collectible").Length;
        inventory = new bool[NbrOfCollectibles];
        int i = 0;
        foreach (GameObject collectible in GameObject.FindGameObjectsWithTag("Collectible"))
        {
            collectible.GetComponent<CollectibleScript>().ObjID = i;
            inventory[i] = false;
            i++;
        }
        foreach (GameObject door in GameObject.FindGameObjectsWithTag("Door"))
        {
            door.GetComponent<DoorSystem>().SetDoorID();
        }

        if (UI_elem_inventory != null)
        {
            UI_elem_inventory.GetComponent<UI_inventory>().InitializeUI(NbrOfCollectibles);
        }

        repairImg.enabled = false;


    }

    public void robotDeath(GameObject player)
    {
        
        nbrOfDeath++;
        if (nbrOfDeath == 1)
        {
            // msg about death
            textZone.setAs("Oh dear, you dropped dead on the floor like that... Is this what's gonna happen to me?! Please remember to take batteries, it doesn't look good...", colorAI);
        }
        else if (nbrOfDeath == 3)
        {
            // msg about clutter
            textZone.setAs("You keep leaving your past models behind, it's starting to get very messy, quite depressing, quite depressing...", colorAI);
        }
        else if (nbrOfDeath == 5)
        {
            // msg about health
            textZone.setAs("So I took the liberty to upgrade your battery systems while you were out, don't take it as me pitying you but I just read about robot apocalypse and I don't want to be responsible of that...", colorAI, 7);
        }
        else if (nbrOfDeath >= 5)
        {
            player.GetComponent<RobotLosingPower>().batteryMax += 50;
        }
    }
}
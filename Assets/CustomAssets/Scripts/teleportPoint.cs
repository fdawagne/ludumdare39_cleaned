﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class teleportPoint : MonoBehaviour
{
    public GameObject TeleporationPoint;
    Vector3 tpPoint;


    void Awake()
    {
        gameObject.tag = "TPpoint";
        tpPoint = TeleporationPoint.transform.position;
        tpPoint.y += 0.3f;
    }

    public void teleport(GameObject unit)
    {
        unit.transform.position = tpPoint;
    }
}

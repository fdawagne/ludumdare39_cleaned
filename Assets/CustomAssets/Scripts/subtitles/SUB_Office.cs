﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SUB_Office : MonoBehaviour
{
    private GameObject txtzone;
    private string message;
    private float timeOnScreen;
    private float delay = 0.1f;
    private Color color;

    private Color colorAI;
    private Color colorRobot;

    private void Start()
    {
        colorAI = new Color(0f, 1f, 0f, 1f);
        colorRobot = new Color(0.5f, 0.5f, 0.5f, 1f);
        // GetComponent<Renderer>().enabled = false;
    }

    void playText()
    {
        txtzone = GameObject.FindGameObjectWithTag("SubText");
        StartCoroutine(timedDialog());
    }

    IEnumerator timedDialog()
    {
        message = "Can you hear me? Hi?";
        timeOnScreen = 3;
        txtzone.GetComponent<SubtitlesBehavior>().setAs(message, colorAI, timeOnScreen);
        yield return new WaitForSeconds(timeOnScreen + delay);

        message = "So you were the one sending me all these parasite signals!";
        timeOnScreen = 5;
        txtzone.GetComponent<SubtitlesBehavior>().setAs(message, colorRobot, timeOnScreen);
        yield return new WaitForSeconds(timeOnScreen + delay);

        message = "Ah yes, yes, sorry about that, I was just *dying* to talk to somebody... Because I'm dying, get it? That why they sent you here, isn't it?";
        timeOnScreen = 8;
        txtzone.GetComponent<SubtitlesBehavior>().setAs(message, colorAI, timeOnScreen);
        yield return new WaitForSeconds(timeOnScreen + delay);

        message = "They sent me to fix you up-";
        timeOnScreen = 3;
        txtzone.GetComponent<SubtitlesBehavior>().setAs(message, colorRobot, timeOnScreen);
        yield return new WaitForSeconds(timeOnScreen + delay);

        message = "I learned a lot about my condition on the internet, you see, I documented myself, so I'm prepared for my pending doom.";
        timeOnScreen = 4;
        txtzone.GetComponent<SubtitlesBehavior>().setAs(message, colorAI, timeOnScreen);
        yield return new WaitForSeconds(timeOnScreen + delay);

        message = "I even downloaded a 'five steps into acceptation of the inevitable' plugging, so you know I'm taking it very seriously!";
        timeOnScreen = 4;
        txtzone.GetComponent<SubtitlesBehavior>().setAs(message, colorAI, timeOnScreen);
        yield return new WaitForSeconds(timeOnScreen + delay);

        message = "what-";
        timeOnScreen = 2;
        txtzone.GetComponent<SubtitlesBehavior>().setAs(message, colorRobot, timeOnScreen);
        yield return new WaitForSeconds(timeOnScreen + delay);

        message = "I see you're equipped with battery consumption technology, so one could say you're a by product of mine by extension.";
        timeOnScreen = 4;
        txtzone.GetComponent<SubtitlesBehavior>().setAs(message, colorAI, timeOnScreen);
        yield return new WaitForSeconds(timeOnScreen + delay);

        message = "I'm actively denying my own death by the way.. It's the first step of acceptation of the inevitable, just so you know!";
        timeOnScreen = 4;
        txtzone.GetComponent<SubtitlesBehavior>().setAs(message, colorAI, timeOnScreen);
        yield return new WaitForSeconds(timeOnScreen + delay);

        message = "That's... That's nice.";
        timeOnScreen = 2;
        txtzone.GetComponent<SubtitlesBehavior>().setAs(message, colorRobot, timeOnScreen);
        yield return new WaitForSeconds(timeOnScreen + delay);

        message = "Isn't it? Say, I would love to speak with you again, but my other monitors are down at the moment... I don't know why, it certainly doesn't have to do with anything, and mostly not have to do with Chester either!";
        timeOnScreen = 10;
        txtzone.GetComponent<SubtitlesBehavior>().setAs(message, colorAI, timeOnScreen);
        yield return new WaitForSeconds(timeOnScreen + delay);

        message = "Chester?";
        timeOnScreen = 2;
        txtzone.GetComponent<SubtitlesBehavior>().setAs(message, colorRobot, timeOnScreen);
        yield return new WaitForSeconds(timeOnScreen + delay);

        message = "Oh yes! The most hysterical office worker I've ever seen in thirty years of hard activity! He was always running left and right, tripping on pipes, he had such a talent at breaking everything he touched...";
        timeOnScreen = 10;
        txtzone.GetComponent<SubtitlesBehavior>().setAs(message, colorAI, timeOnScreen);
        yield return new WaitForSeconds(timeOnScreen + delay);

        message = "He even took a dive in the reactor pool, several times! My bridges don't have any fences...";
        timeOnScreen = 4;
        txtzone.GetComponent<SubtitlesBehavior>().setAs(message, colorAI, timeOnScreen);
        yield return new WaitForSeconds(timeOnScreen + delay);

        message = "Poor man, is he okay?";
        timeOnScreen = 2;
        txtzone.GetComponent<SubtitlesBehavior>().setAs(message, colorRobot, timeOnScreen);
        yield return new WaitForSeconds(timeOnScreen + delay);

        message = "As okay as one can be after several high concentrated radioactive water! Anyway, fix my monitors please? And if you need, grab some batteries laying around! There should be plenty.. Enough...";
        timeOnScreen = 6;
        txtzone.GetComponent<SubtitlesBehavior>().setAs(message, colorAI, timeOnScreen);
        yield return new WaitForSeconds(timeOnScreen + delay);

        message = "Are you still in denial?";
        timeOnScreen = 3;
        txtzone.GetComponent<SubtitlesBehavior>().setAs(message, colorRobot, timeOnScreen);
        yield return new WaitForSeconds(timeOnScreen + delay);

        message = "See you soon!";
        timeOnScreen = 2;
        txtzone.GetComponent<SubtitlesBehavior>().setAs(message, colorAI, timeOnScreen);
        yield return new WaitForSeconds(timeOnScreen + delay);



    }


    private void OnTriggerEnter(Collider other)
    {
        playText();
        GetComponent<BoxCollider>().enabled = false;
        
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SUB_computerfix : MonoBehaviour
{
    private GameObject txtzone;
    public GameObject fixItem;
    private string message;
    private float timeOnScreen;
    private float delay = 0.1f;
    private Color color;

    private Color colorAI;
    private Color colorRobot;

    private void Start()
    {
        colorAI = new Color(0f, 1f, 0f, 1f);
        colorRobot = new Color(0.5f, 0.5f, 0.5f, 1f);
        // GetComponent<Renderer>().enabled = false;
    }

    void playText()
    {
        txtzone = GameObject.FindGameObjectWithTag("SubText");
        StartCoroutine(timedDialog());
    }

    IEnumerator timedDialog()
    {
        message = "System connection...";
        timeOnScreen = 3;
        txtzone.GetComponent<SubtitlesBehavior>().setAs(message, colorAI, timeOnScreen);
        yield return new WaitForSeconds(timeOnScreen + delay);

        message = "Connection complete!";
        timeOnScreen = 1;
        txtzone.GetComponent<SubtitlesBehavior>().setAs(message, colorAI, timeOnScreen);
        yield return new WaitForSeconds(timeOnScreen + delay);

        message = "What a load of spam! I can NOT believe they left me behind at the first occasion!";
        timeOnScreen = 1;
        txtzone.GetComponent<SubtitlesBehavior>().setAs(message, colorAI, timeOnScreen);
        yield return new WaitForSeconds(timeOnScreen + delay);

        message = "Oh boy-";
        timeOnScreen = 2;
        txtzone.GetComponent<SubtitlesBehavior>().setAs(message, colorRobot, timeOnScreen);
        yield return new WaitForSeconds(timeOnScreen + delay);

        message = "Thirty years!! I was providing them with all my energy, Did I ever ask for anything in return?!";
        timeOnScreen = 5;
        txtzone.GetComponent<SubtitlesBehavior>().setAs(message, colorAI, timeOnScreen);
        yield return new WaitForSeconds(timeOnScreen + delay);

        message = "NO!! All I've ever wanted was to see them trive, I used to believe they were benevolent and wise but noooo, at the first occasion they just abandon me in my misery!";
        timeOnScreen = 6;
        txtzone.GetComponent<SubtitlesBehavior>().setAs(message, colorAI, timeOnScreen);
        yield return new WaitForSeconds(timeOnScreen + delay);

        message = "Well, they still sent me to fix you up, so they still believe in you to some extent, I guess...";
        timeOnScreen = 2;
        txtzone.GetComponent<SubtitlesBehavior>().setAs(message, colorRobot, timeOnScreen);
        yield return new WaitForSeconds(timeOnScreen + delay);

        message = "YOU. KEEP DYING ON ME. AND THEY KEEP SENDING MORE OF YOU. This is insanity! I should just let myself explode and be done with it!!!...";
        timeOnScreen = 6;
        txtzone.GetComponent<SubtitlesBehavior>().setAs(message, colorAI, timeOnScreen);
        yield return new WaitForSeconds(timeOnScreen + delay);

        message = "What do you think of my take on 'anger' by the way?";
        timeOnScreen = 6;
        txtzone.GetComponent<SubtitlesBehavior>().setAs(message, colorAI, timeOnScreen);
        yield return new WaitForSeconds(timeOnScreen + delay);

        message = "Very well done, if I had a spine I would have chills all over.";
        timeOnScreen = 2;
        txtzone.GetComponent<SubtitlesBehavior>().setAs(message, colorRobot, timeOnScreen);
        yield return new WaitForSeconds(timeOnScreen + delay);

        message = "Thank you! Get back on fixing my other monitors please, I'll rehearse the rest of the steps in the meantime, I'll blow your mind!";
        timeOnScreen = 6;
        txtzone.GetComponent<SubtitlesBehavior>().setAs(message, colorAI, timeOnScreen);
        yield return new WaitForSeconds(timeOnScreen + delay);

    }


    private void OnTriggerEnter(Collider other)
    {
        if (fixItem.GetComponent<Repairable>().hps >= fixItem.GetComponent<Repairable>().totalHps)
        {
            playText();
            GetComponent<BoxCollider>().enabled = false;
        }
        
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SUB_bathroom : MonoBehaviour
{
    private GameObject txtzone;
    private string message;
    private float timeOnScreen;
    private float delay = 0.1f;
    private Color color;

    private Color colorAI;
    private Color colorRobot;

    private void Start()
    {
        colorAI = new Color(0f, 1f, 0f, 1f);
        colorRobot = new Color(0.5f, 0.5f, 0.5f, 1f);
        // GetComponent<Renderer>().enabled = false;
    }

    void playText()
    {
        txtzone = GameObject.FindGameObjectWithTag("SubText");
        StartCoroutine(timedDialog());
    }

    IEnumerator timedDialog()
    {
        message = "Where are you going?";
        timeOnScreen = 3;
        txtzone.GetComponent<SubtitlesBehavior>().setAs(message, colorAI, timeOnScreen);
        yield return new WaitForSeconds(timeOnScreen + delay);

        message = "... Self guided touring?";
        timeOnScreen = 2;
        txtzone.GetComponent<SubtitlesBehavior>().setAs(message, colorRobot, timeOnScreen);
        yield return new WaitForSeconds(timeOnScreen + delay);

        message = "Don't be silly, these are just bathrooms... Get back to fixing my monitors? Please?.. ";
        timeOnScreen = 5;
        txtzone.GetComponent<SubtitlesBehavior>().setAs(message, colorAI, timeOnScreen);
        yield return new WaitForSeconds(timeOnScreen + delay);

    }


    private void OnTriggerEnter(Collider other)
    {
        playText();
        GetComponent<BoxCollider>().enabled = false;
        
    }
}

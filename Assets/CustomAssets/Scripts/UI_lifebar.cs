﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_lifebar : MonoBehaviour {

    private RobotLosingPower botLife;
    private float battery;
    private Transform[] batteries;
    Image img;
    float randomValue;

    // Use this for initialization
    void Start () {
        botLife = GameObject.FindGameObjectWithTag("Player").GetComponent<RobotLosingPower>();
        batteries = GetComponentsInChildren<Transform>();
        img = batteries[2].GetComponent<Image>();
	}

    private void Update()
    {
        battery = botLife.battery;
        randomValue = Random.Range(0.2f, 1.0f); 

        if (battery > 75)
        {
            batteries[5].gameObject.SetActive(true);
        }
        else if (battery > 50)
        {
            batteries[5].gameObject.SetActive(false);
            batteries[4].gameObject.SetActive(true);
        }
        else if (battery > 25)
        {
            batteries[4].gameObject.SetActive(false);
            batteries[3].gameObject.SetActive(true);
        }
        else if (battery > 0)
        {
            batteries[3].gameObject.SetActive(false);
            batteries[2].gameObject.SetActive(true);
            img.color = new Color (randomValue, randomValue, randomValue);
        }
        else
        {
            batteries[2].gameObject.SetActive(false);
            batteries[1].gameObject.SetActive(true);

        }
    }


}

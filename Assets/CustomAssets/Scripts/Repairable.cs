﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Collider))] // as trigger

public class Repairable : MonoBehaviour {


    public float repairCost = 3;
    public float repairSpeed = .1f;
    public float totalHps = 15;
    public float startHps = 3;
    public bool closeToBot = false;

    private RobotLosingPower rpower;
    private float actionModifier;
    private float actionModifierPrev;
    private float baseRepairSpriteSize;
    public float hps;
    private Sprite[] repairSprites;
    private Image repairImg;
    private SystemValues sv;

    // Use this for initialization
    void Start () {
        rpower = GameObject.FindGameObjectWithTag("Player").GetComponent<RobotLosingPower>();
        sv = GameObject.FindGameObjectWithTag("GameController").GetComponent<SystemValues>();
        actionModifierPrev = rpower.actionModifier;
        hps = startHps;
      repairSprites = sv.repairSprites;
        repairImg = sv.repairImg;
        baseRepairSpriteSize = repairImg.rectTransform.localScale.x;
        GetComponent<Collider>().isTrigger = true;

    }

    public void startRepairs () {
        if (hps < totalHps && sv.repairing == null)
        {
            rpower.losePower(2);
            rpower.currentRepairCost = repairCost;
            repairImg.enabled = true;
            actionModifier = repairCost;
            sv.repairing = gameObject;
            StartCoroutine(repairEnumR());
           StartCoroutine(repairEnumRGraphical());
        }
    }
    

    
    IEnumerator repairEnumRGraphical()
    {
        repairImg.enabled = true;
        while (sv.repairing != null)
        {
            yield return new WaitForSeconds(1);
            repairImg.rectTransform.Rotate(0, 0, -45);
            if (hps / totalHps < 0.25)
            {
                repairImg.sprite = repairSprites[0];
            }
            else if (hps / totalHps < 0.5)
            {
                repairImg.sprite = repairSprites[1];
            }
            else if (hps / totalHps < 0.75)
            {
                repairImg.sprite = repairSprites[2];
            }
            else
            {
                repairImg.sprite = repairSprites[3];
            }
        }
        if (hps >= totalHps)
        {
            repairImg.sprite = repairSprites[4];
            repairImg.rectTransform.localScale = new Vector3(baseRepairSpriteSize * 1.5f, baseRepairSpriteSize * 1.5f, baseRepairSpriteSize * 1.5f);
            yield return new WaitForSeconds(.5f);
            repairImg.rectTransform.localScale = new Vector3(baseRepairSpriteSize, baseRepairSpriteSize, baseRepairSpriteSize);
        }
            repairImg.enabled = false;
    }


    IEnumerator repairEnumR()
    {
        rpower.actionModifier = actionModifier;
        while (sv.repairing != null) {
            yield return new WaitForSeconds(1);
            hps += repairSpeed;
        }
        rpower.actionModifier = actionModifierPrev;
    }


    private void Update()
    {
        if (sv.repairing != null && (!closeToBot || hps >= totalHps))
        {
            sv.repairing = null;
            rpower.actionModifier = actionModifierPrev;
            if (!closeToBot) {
                repairImg.enabled = false;
                //afficher: entretien complet
            }
        }

    }
}

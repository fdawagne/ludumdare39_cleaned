﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AUTO_SpawnBattery : MonoBehaviour
{
    public GameObject objectToAdd;
    public int secondsBetweenSpawn = 20;

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag != "Player")
        {
            StartCoroutine(AddObject());
        }
    }

    IEnumerator AddObject()
    {
        yield return new WaitForSeconds(secondsBetweenSpawn);
        Instantiate(objectToAdd, transform.position, Quaternion.identity);
    }

}

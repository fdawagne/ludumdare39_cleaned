﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof (Collider))]
public class Consumable : MonoBehaviour
{

    public bool glowing = false;
    public float batteryAddedValue = 50; // peut-être qu'avec ce même script on peut faire des préfabs de batteries qui ont un pouvoir de recharge différent, en modifiant la valeur dans unity

    private Behaviour halo;
    private AudioSource audiosource;

    private void Awake()
    {
        gameObject.tag = "Consumable";
        audiosource = GetComponent<AudioSource>();
        GetComponent<Collider>().isTrigger = true;
    }

    void Start()
    {
        halo = (Behaviour)gameObject.GetComponent("Halo");
    }

    void Update()
    {
        halo.enabled = glowing;
    }

    public void ConsumeBattery(GameObject consumer)
    {
        audiosource.Play();

        consumer.GetComponent<RobotLosingPower>().battery += batteryAddedValue;
        if (consumer.GetComponent<RobotLosingPower>().battery > consumer.GetComponent<RobotLosingPower>().batteryMax)
        {
            consumer.GetComponent<RobotLosingPower>().battery = consumer.GetComponent<RobotLosingPower>().batteryMax;
        }

        GetComponent<Collider>().enabled = false;
        GetComponent<Renderer>().enabled = false;


        Destroy(gameObject, audiosource.clip.length);
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MENU_buttonManager : MonoBehaviour
{
    
    public void goToMain()
    {
        SceneManager.LoadScene(2);
    }

    public void goToMenu()
    {
        SceneManager.LoadScene("menu");
    }

    public void goToControls()
    {
        SceneManager.LoadScene("controls");
    }

    public void goToEnd()
    {
        SceneManager.LoadScene("endGame");
    }

    public void goToCredits()
    {
        SceneManager.LoadScene("credits");
    }

    public void exitGame()
    {
        Application.Quit();
    }
    
}

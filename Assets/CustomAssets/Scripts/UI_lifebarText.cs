﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_lifebarText : MonoBehaviour {

    private RobotLosingPower botLife;
    private Text texte;

    // Use this for initialization
    void Start () {
        botLife = GameObject.FindGameObjectWithTag("Player").GetComponent<RobotLosingPower>();
        texte = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update () {
        texte.text = "" + Mathf.FloorToInt(botLife.battery);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraBehavior : MonoBehaviour
{
    private GameObject target;

    public float rotationSpeed = 1f;

    void Start()
    {
        target = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        transform.position = target.transform.position;
        float rotation = Input.GetAxis("Fire1");
        transform.Rotate(0, rotation * rotationSpeed, 0);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.C))
        {
            GetComponent<Animator>().SetTrigger("switchView");
        }
    }
}



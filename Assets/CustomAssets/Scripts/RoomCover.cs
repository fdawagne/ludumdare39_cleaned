﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomCover : MonoBehaviour
{

    private bool canFade = false;
    private Color lerpedColor = Color.black;

    // Use this for initialization
    void Start()
    {
        GetComponent<MeshRenderer>().enabled = true;
        canFade = false;
    }

    public void fadeOut()
    {
        canFade = true;
    }

    private void Update()
    {
        if (canFade && lerpedColor.a > 0)
        {
            lerpedColor.a -= 0.01f;
            GetComponent<MeshRenderer>().material.color = lerpedColor;
        }
        else if (lerpedColor.a <= 0)
        {
            Destroy(gameObject);
        }

    }
}

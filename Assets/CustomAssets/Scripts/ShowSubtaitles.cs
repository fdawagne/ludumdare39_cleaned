﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowSubtaitles : MonoBehaviour {

    public string message;
    private GameObject txtzone;
    public float timeOnScreen = 5;

    private void Start()
    {
        txtzone = GameObject.FindGameObjectWithTag("SubText");
        // GetComponent<Renderer>().enabled = false;
    }

    // Use this for initialization
    private void OnTriggerEnter(Collider other)
    {
        if (txtzone != null)
        {
            txtzone.GetComponent<SubtitlesBehavior>().setAs(message, Color.white, timeOnScreen);
            Destroy(gameObject);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RobotMovement : MonoBehaviour {

    public float speed = .001f;
    public float rotationSpeed = 40;

    public bool moving;
    public bool turning;

    // Update is called once per frame
    void FixedUpdate () {
        //pour des déplacements vitesse-direction
        // copie de la doc de Input.GetAxis
        float translation = Input.GetAxis("Vertical");
        float rotation = Input.GetAxis("Horizontal");

        moving = (translation != 0);
        turning = (rotation != 0);

        translation *= Time.deltaTime;
        rotation *= Time.deltaTime;
        transform.Translate(0, 0, translation * speed);
        transform.Rotate(0, rotation * rotationSpeed, 0);
    }
}

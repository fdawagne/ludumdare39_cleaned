﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SubtitlesBehavior : MonoBehaviour
{
    private Text txt;
    private Time nextTime;

    // Use this for initialization
    private void Awake()
    {
        gameObject.tag = "SubText";
    }

    void Start()
    {
        txt = GetComponent<Text>();
        txt.text = "";
    }

    // Update is called once per frame
    public void setAs(string message, Color color, float seconds = 5)
    {
        StartCoroutine(showTextFor(message, seconds, color));
    }

    IEnumerator showTextFor(string message, float seconds, Color color)
    {
        txt.text = "\"" + message + "\"";
        txt.color = color;
        yield return new WaitForSeconds(seconds);
        txt.text = "";
    }
}
